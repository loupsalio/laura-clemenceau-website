'use strict';
var main = require('../controllers/mainController'),
  news = require('../controllers/newsController'),
  configs = require('../controllers/configsController'),
  passport = require("passport")

module.exports = function (app) {

  app.route('/')
    .get(main.index);

  app.route("/send_contact_mail")
    .post(main.contact_mail)

  app.route('/login').post(
      passport.authenticate('local', {
        failureRedirect: '/login',
        failureFlash: true
      }),
      function (req, res) {
        res.redirect('/admin/news');
      })
    .get(main.login)

  app.route('/admin')
    .get((req, res) => {
      res.redirect("/login")
    })

  app.route('/logout').post(main.logout).get(main.logout);

  app.route('/admin/news')
    .get(news.news);

  app.route("/add/news")
    .post(news.add_news)

  app.route("/remove/news")
    .post(news.del_news)

  app.route("/edit/news")
    .post(news.edit_news)

  app.route("/admin/config")
    .get(configs.configs)

  app.route("/edit/config")
    .post(configs.edit_configs)
};