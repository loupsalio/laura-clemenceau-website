'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ConfigsSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  value: {
    type: String,
    required: true
  },
  created_date: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model('Configs', ConfigsSchema);