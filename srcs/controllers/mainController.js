"use strict";
var pug = require("pug"),
    path = require("path"),
    mongoose = require("mongoose"),
    moment = require("moment"),
    nodemailer = require("nodemailer"),
    news = mongoose.model("News"),
    Configs = mongoose.model("Configs");

const configuration_list = ["application_name", "presentation_value", "accroche_value", "button_value", "welcome_value", "description_value", "mail_contact_value"]

/**
 * Main page 
 * @route '/' 
 * @param {Request} req - Origin request
 * @param {Response} res - Response request
 */

exports.index = (req, res) => {
    moment.locale("fr");
    news.find((err, elem) => {
        if (err)
            return res.json({
                error: err
            });
        elem.forEach(element => {
            var tmp = moment(element.created_date).format("Do MMMM YYYY, hh:mm");
            element.date = tmp;
        });
        Configs.find((err, list) => {
            if (err)
                return res.json({
                    error: err
                });
            var body = pug.renderFile(path.resolve("public/views/main.pug"), {
                list: elem,
                configs: list,
                messages: req.flash()
            });
            return res.send(body);
        });
    });
};

/**
 * Login page
 * @route '//login'-'/admin' 
 * @param {Request} req - Origin request
 * @param {Response} res - Response request
 */
exports.login = (req, res) => {
    if (req.user)
        return res.redirect("/admin/news");

    var body = pug.renderFile(path.resolve("public/views/login.pug"), {
        messages: req.flash()
    });
    return res.send(body);
};

/**
 * Disconnect the user
 * @route '/logout' 
 * @param {Request} req - Origin request
 * @param {Response} res - Response request
 */
exports.logout = function (req, res) {
    req.logout();
    req.flash("notify", "Disconnected");
    res.redirect("/");
};

/**
 * Send mail to unnaturalweb@gmail.com
 * @route '/send_contact_mail' 
 * @param {Request} req - Origin request
 * @param {Response} res - Response request
 */
exports.contact_mail = (req, res) => {
    var transporter = nodemailer.createTransport({
        service: "gmail",
        auth: {
            user: "unnaturalweb@gmail.com",
            pass: "starwolf44"
        }
    });
    var mailOptions = {
        from: '"' + req.body.name + '" <unnaturalweb@gmail.com>',
        to: "unnaturalweb@gmail.com",
        subject: req.body.subject,
        html: "<p><b>Mail : </b>" +
            req.body.mail +
            "</p>" +
            "<p><b>Message : </b>" +
            req.body.message +
            "</p>"
    };
    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            return console.log(error);
        }
        console.log("Message sent: " + info.response);
        req.flash("notify", "Message envoyé");
        res.redirect("/");
    });

    transporter.close();
};