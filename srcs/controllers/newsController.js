"use strict";
var pug = require("pug"),
    path = require("path"),
    mongoose = require("mongoose"),
    moment = require("moment"),
    save = require(process.env.base_directory + "/save"),
    news = mongoose.model("News");

/**
 * News CRUD page
 * @route '/admin/news' 
 * @param {Request} req - Origin request
 * @param {Response} res - Response request
 */
exports.news = (req, res) => {
    if (!req.user) {
        req.flash("error", "You are not connected");
        return res.redirect("/login");
    }
    moment.locale("fr");
    news.find((err, elem) => {
        if (err)
            return res.json({
                error: err
            });
        elem.forEach(element => {
            var tmp = moment(element.created_date).format("Do MMMM YYYY, hh:mm");
            element.date = tmp;
        });
        var body = pug.renderFile(path.resolve("public/views/crud_news.pug"), {
            list: elem,
            messages: req.flash(),
            user: req.user
        });
        return res.send(body);
    });
};

/**
 * Add a news object in Database
 * @route '/add/news' 
 * @param {Request} req - Origin request
 * @param {Response} res - Response request
 */
exports.add_news = (req, res) => {
    if (!req.user) {
        req.flash("error", "You are not connected");
        return res.redirect("/login");
    }
    var tmp = new news(req.body);
    tmp.save((err, value) => {
        save.serealize();
    });
    return res.redirect("/admin/news");
};

/**
 * remove a news object in Database
 * @route '/remove/news' 
 * @param {Request} req - Origin request
 * @param {Response} res - Response request
 */
exports.del_news = (req, res) => {
    if (!req.user) {
        req.flash("error", "You are not connected");
        return res.redirect("/login");
    }
    news.findOneAndDelete({
        title: req.body.title,
        content: req.body.content,
        autor: req.body.autor
    }, (err) => {
        if (err)
            console.log(err)
        save.serealize();
    });

    return res.redirect("/admin/news");
};

/**
 * edit a news object in Database
 * @route '/edit/news' 
 * @param {Request} req - Origin request
 * @param {Response} res - Response request
 */
exports.edit_news = (req, res) => {
    if (!req.user) {
        req.flash("error", "You are not connected");
        return res.redirect("/login");
    }
    news.findOneAndReplace({
        title: req.body.modal_title,
        content: req.body.modal_content,
        autor: req.body.modal_autor
    }, {
            title: req.body.title,
            content: req.body.content,
            autor: req.body.autor
        }, (err) => {
            if (err)
                console.log(err)
            save.serealize();
        });
    return res.redirect("/admin/news");
};