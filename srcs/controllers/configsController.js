"use strict";
var pug = require("pug"),
    path = require("path"),
    mongoose = require("mongoose"),
    moment = require("moment"),
    save = require(process.env.base_directory + "/save"),
    Configs = mongoose.model("Configs");
const configuration_list = ["application_name", "presentation_value", "accroche_value", "button_value", "welcome_value", "description_value", "mail_contact_value"]

/**
 * Configuration init function
 */
exports.init = () => {
    configuration_list.forEach(element => {
        Configs.findOne({
            name: element
        }, (err, value) => {
            if (err)
                return console.log("Configuration error : " + err)
            if (!value)
                Configs({
                    name: element,
                    value: element
                }).save((err) => {
                    console.log("-< Created [" + element + "]")
                })
            else
                console.log("-> Loaded [" + element + "]")
        })
    });
}

/**
 * Admin base configuration page
 * @route '/admin/config' 
 * @param {Request} req - Origin request
 * @param {Response} res - Response request
 */
exports.configs = (req, res) => {
    if (!req.user) {
        req.flash("error", "You are not connected");
        return res.redirect("/login");
    }
    moment.locale("fr");
    Configs.find((err, list) => {
        if (err)
            return res.json({
                error: err
            });
        var body = pug.renderFile(path.resolve("public/views/configurations.pug"), {
            configs: list,
            messages: req.flash(),
            user: req.user
        });
        return res.send(body);
    });
};

/**
 * Edit base configurations
 * @route '/edit/config' 
 * @param {Request} req - Origin request
 * @param {Response} res - Response request
 */
exports.edit_configs = (req, res) => {
    if (!req.user) {
        req.flash("error", "You are not connected");
        return res.redirect("/login");
    }
    Configs.findOneAndReplace({
        name: configuration_list[0]
    }, {
            $set: {
                name: configuration_list[0],
                value: req.body.application_name
            }
        }, (err, elem) => {
            if (err)
                console.log(err)
        })
    Configs.findOneAndReplace({
        name: configuration_list[1]
    }, {
            $set: {
                name: configuration_list[1],
                value: req.body.presentation_value
            }
        }, (err, elem) => {
            if (err)
                console.log(err)
        })
    Configs.findOneAndReplace({
        name: configuration_list[2]
    }, {
            $set: {
                name: configuration_list[2],
                value: req.body.accroche_value
            }
        }, (err, elem) => {
            if (err)
                console.log(err)
        })
    Configs.findOneAndReplace({
        name: configuration_list[3]
    }, {
            $set: {
                name: configuration_list[3],
                value: req.body.button_value
            }
        }, (err, elem) => {
            if (err)
                console.log(err)
        })
    Configs.findOneAndReplace({
        name: configuration_list[4]
    }, {
            $set: {
                name: configuration_list[4],
                value: req.body.welcome_value
            }
        }, (err, elem) => {
            if (err)
                console.log(err)
        })
    Configs.findOneAndReplace({
        name: configuration_list[5]
    }, {
            $set: {
                value: req.body.description_value,
                name: configuration_list[5]
            }
        }, (err, elem) => {
            if (err)
                console.log(err)
        })
    Configs.findOneAndReplace({
        name: configuration_list[6]
    }, {
            $set: {
                value: req.body.mail_contact_value,
                name: configuration_list[6]
            }
        }, (err, elem) => {
            if (err)
                console.log(err)
        })
    save.serealize();
    req.flash("notify", "Configurations saved")
    return res.redirect("/admin/config");
};