# lau_website web project

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

```
npm / Nodejs
```

### Installing

First check you installed the requisites, then clone the repository and install the dependencies :

```bash
npm install
```

Now you can run the project:

```bash
npm start
``` 

## Authors

* **Lucas CLEMENCEAU** - *Developer* - [Loupsalio](https://gitlab.com/loupsalio)
