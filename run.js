var express = require("express"),
  app = express(),
  port = process.env.PORT || 5300,
  mongoose = require("mongoose"),
  bodyParser = require("body-parser"),
  flash = require("connect-flash"),
  session = require("express-session"),
  cookieparser = require("cookie-parser"),
  morgan = require("morgan");

/* Initialisation */

console.log("################################");
console.log("#   lau_website Web started   ##");
console.log("################################");

// Env and Auth configuration

require("dotenv").config();
process.env.base_directory = __dirname;
app.use(express.static(__dirname + "/ressources"));

app.use(cookieparser("secret"));
app.use(
  session({
    secret: "keyboard wolf",
    name: "session_cookie",
    // store: sessionStore, // connect-mongo session store
    proxy: true,
    resave: true,
    saveUninitialized: true
  })
);
app.use(flash());

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);
if (process.env.ENVIRONEMENT == "dev") app.use(morgan("dev"));

require("./srcs/models/userModel");
require("./srcs/models/newsModel");
require("./srcs/models/configModel");

var auth = require("./srcs/auth");
auth(app);

// MongoDB configuration

mongoose.set("useCreateIndex", true);
mongoose.Promise = global.Promise;
if (!process.env.MONGO_URI)
  process.env.MONGO_URI = "mongodb://localhost/lau_ws";
mongoose.connect(process.env.MONGO_URI, {
  useNewUrlParser: true,
  // retry to connect for 60 times
  reconnectTries: 6000,
  // wait 1 second before retrying
  reconnectInterval: 3000
});

var user = mongoose.model("Users");
user.deleteMany(err => {
  if (err) return console.log(err);
  user.create(
    {
      username: "laura",
      password: "master"
    },
    (err, user) => {
      if (err) console.log(err);
      console.log("admin account ready");
    }
  );
});

require("./srcs/controllers/configsController").init(); // run init configurations

// Cross origin header control (Disabled)

app.use(function(request, response, next) {
  var allowedOrigins = [
    "https://link.under-wolf.eu",
    "https://www.link.under-wolf.eu"
  ];
  var origin = request.headers.origin;
  if (allowedOrigins.indexOf(origin) > -1) {
    response.setHeader("Access-Control-Allow-Origin", origin);
  }
  response.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

var routes = require("./srcs/routes/mainroutes");
routes(app);

// 404 Redirect

app.use(function(req, res) {
  res.redirect("/");
});

// Run application listening

app.listen(port);

// Route list debug

console.log("Listening on : http://localhost:" + port);
console.log("________________________________");
