FROM node:latest

# Définir le répertoire de travail de l'applicaiton à /app
RUN mkdir -p /app/lau_website
WORKDIR /app/lau_website

# Copier le contenu du répertoire courant dans le conteneur à l'adresse /app
ADD . /app/lau_website

# Définir les variable d'environnement
ENV NAME lau_website

COPY package.json /app/lau_website/
RUN npm install --quiet

COPY . /app

# Rendre le port 5300 accessible à l'extérieur du conteneur
EXPOSE 5300

CMD [ "npm", "start" ]
