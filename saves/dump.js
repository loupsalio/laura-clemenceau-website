var fs = require('fs');

var mongoose = require('mongoose');

mongoose.Promise = global.Promise;
if (!process.env.MONGO_URI)
    process.env.MONGO_URI = 'mongodb://localhost/lau-website'
mongoose.connect(process.env.MONGO_URI, {
    useNewUrlParser: true
});

require('./models/newsModel');
require('./models/configModel');

var Config = mongoose.model("Configs");
var News = mongoose.model('News');

require('dotenv').config();

fs.readFile('saves/version.txt', 'utf8', function (erreur, donnees) {
    if (erreur)
        throw erreur;
    fs.readFile("saves/" + donnees + '.txt', 'utf8', function (erreur, don) {
        if (erreur)
            throw erreur;
        var monJson = JSON.parse(don);
        News.remove(function (err) {
            monJson[0].forEach(element => {
                var tmp = News(element)
                tmp.save(() => {
                    console.log("News " + tmp + " saved")
                })
            });
        })

        Config.remove(function (err) {
            monJson[1].forEach(element => {
                var tmp = Config(element)
                tmp.save(() => {
                    console.log("Config " + tmp + " saved")
                })
            });
        });
    });
});
setTimeout(function () {
    mongoose.disconnect();
}, 4000);
