
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>back-up page</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="shortcut icon" type="image/png" href="/fav.png"/>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  </head>
  <body>
  <?php if (isset($_POST["content"])) {
    $value = $_POST["content"];
    file_put_contents("saves/version.txt", $value);
    exec("node dump.js");
  //   echo "<div class=\"alert alert-info\">
  //   <strong>Info!</strong> $value version dumped.
  // </div>";
    header('Location: /');
  }
  ?>

  <table class="table table-sm table-striped table-dark">
  <thead>
    <tr>
      <th scope="col">Backup</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
  <?php 

  $version = file_get_contents("saves/version.txt");

  $dircontents = scandir("./saves/");
  $dircontents = array_reverse($dircontents);
  foreach ($dircontents as $file) {
    echo '<tr>';
    $extension = pathinfo($file, PATHINFO_EXTENSION);
    if ($extension == 'txt' && $file != "version.txt") {
      $file = str_replace(".txt", "", $file);
      if ($version != $file)
        echo "<th>&#9965; $file </th>";
      else
        echo "<th>&#9967; $file </th>";
      echo "<th><form method=\"POST\" action=\"\">
      <input type=\"hidden\" name=\"content\" value=\"$file\"/>";
      if ($version != $file)
        echo "<input class=\"btn-small btn-warning\" onClick=\"this.form.submit();this.disabled=true; this.value='Dumping…';\" type=\"submit\" value=\"Dump\"/>";
      else
        echo "<p class=\".text-warning\">Actual version</p>";
      echo "</form>";
      echo "</th>";
    }
    echo '</tr>';
  }
  ?>
  </tbody>
</table>
  </body>
</html>